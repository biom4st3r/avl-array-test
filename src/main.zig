const std = @import("std");
const avl = @cImport(@cInclude("avl_tree.h"));

pub fn main() !void {
    const tree = avl.avltree_new(@ptrCast(&struct {
        fn cmp(a: ?*const usize, b: ?*const usize) callconv(.C) c_long {
            return @intCast(@as(isize, @intCast(b.?.*)) - @as(isize, @intCast(a.?.*)));
        }
    }.cmp));
    const numbers = [_]usize{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    avl.avltree_insert(tree, @ptrCast(@constCast(&numbers[6])), null);
    avl.avltree_insert(tree, @ptrCast(@constCast(&numbers[5])), null);
    avl.avltree_insert(tree, @ptrCast(@constCast(&numbers[4])), null);
    avl.avltree_insert(tree, @ptrCast(@constCast(&numbers[3])), null);
    avl.avltree_insert(tree, @ptrCast(@constCast(&numbers[2])), null);
    avl.avltree_insert(tree, @ptrCast(@constCast(&numbers[1])), null);
    std.log.debug("\n", .{});
    for (0..64) |i| {
        const k = tree[0].nodes[i].key;
        std.log.debug("k: {d}", .{
            if (k == null) @as(usize, @intCast(0)) else @as(*usize, @ptrCast(@alignCast(k))).*,
        });
    }
}
