#include "../libs/array-avl-tree/avl_tree.h"
#include <stdio.h>

long cmp(int *a, int *b) {
  return *b - *a;
}

int main(int argc, char** argv) {
  avltree_t *tree = avltree_new((long (*)(const void*, const void*))cmp);
  int data[] = {1,2,3,4,5,6,7};
  for (int i = 0; i < 7; i++) {
    avltree_insert(tree, data + (i), NULL);
  }
  for(int i = 0; i < 7; i++) {
    void *k = tree->nodes[i].key;
    if (k == NULL) printf("k: %d\n", 0);
    else printf("k: %d\n", *((int*)k));
  }
}
